output "web_vm_ip" {
  value = azurerm_network_interface.azurevm1nic.ip_configuration[0].private_ip_address
}

output "db_vm_ip" {
  value = azurerm_network_interface.azurevm2nic.ip_configuration[0].private_ip_address
}

output "web_vm_public_ip" {
  value = azurerm_public_ip.azurevm1publicip.ip_address
}

output "db_vm_public_ip" {
  value = azurerm_public_ip.azurevm2publicip.ip_address
}
