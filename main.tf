

#build azure resource group
resource "azurerm_resource_group" "main" {
  name = var.resource_group_name2
  location = var.location2
  
}

resource "azurerm_virtual_network" "mainnetwork" {
  name = "vnet_terraform"
  location = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  address_space = var.vnet_address_space2
}

#build devsubnet and link to mainnetwork virtual network
resource "azurerm_subnet" "web_subnet" {
  name = "web-subnet"
  resource_group_name = azurerm_resource_group.main.name
  address_prefixes = var.web_subnet_address_prefixes2
  virtual_network_name = azurerm_virtual_network.mainnetwork.name
}

#build testsubnet and link to mainnetwork virtual network
resource "azurerm_subnet" "db_subnet" {
  name = "db-subnet"
  resource_group_name = azurerm_resource_group.main.name
  address_prefixes = var.db_subnet_address_prefixes2
  virtual_network_name = azurerm_virtual_network.mainnetwork.name
}

# Defining the Network Security Group
resource "azurerm_network_security_group" "example" {
  name                = "nsg-web"
  location            = var.location2
  resource_group_name = azurerm_resource_group.main.name

  # Updated SSH rule with your PC's IP address
  security_rule {
    name                       = "SSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = var.allowed_ssh_ip2  
    destination_address_prefix = "*"
  }

  # New rules for ports 80, 8080, and 5000 (unchanged)
  security_rule {
    name                       = "WebTraffic1"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "WebTraffic2"
    priority                   = 102
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "WebTraffic3"
    priority                   = 103
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_range     = "5000"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "db_nsg"{
  name                = "nsg-db"
  location            = var.location2
  resource_group_name = azurerm_resource_group.main.name

  security_rule {
    name                       = "postgresql-traffic"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5432"
    source_address_prefix      = var.web_subnet_address_prefixes2[0]  # the subnet of the web in vnet
    destination_address_prefix = "*"
  }
   security_rule {
    name                       = "postgresql-traffic2"
    priority                   = 101
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = var.allowed_ssh_ip2  # my computer ip address
    destination_address_prefix = "*"
  }
}

#Associating the Subnet with the NSG for Web Subnet
resource "azurerm_subnet_network_security_group_association" "web_association" {
  subnet_id                  = azurerm_subnet.web_subnet.id
  network_security_group_id = azurerm_network_security_group.example.id
}

#Associating the Subnet with the NSG for Database Subnet
resource "azurerm_subnet_network_security_group_association" "db_association" {
  subnet_id                  = azurerm_subnet.db_subnet.id
  network_security_group_id = azurerm_network_security_group.db_nsg.id
}


resource "azurerm_public_ip" "azurevm1publicip" {
  name                = "pip-web"
  location            = var.location2
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
  sku                 = var.public_ip_sku2
}

resource "azurerm_network_interface" "azurevm1nic" {
  name                = "web-nic"
  location            = var.location2
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "ipconfig-web"
    subnet_id                     = azurerm_subnet.web_subnet.id  # Corrected reference
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.azurevm1publicip.id
  }
}

resource "azurerm_linux_virtual_machine" "web-vm" {
  name                            = "web-vm"
  resource_group_name             = azurerm_resource_group.main.name
  location                        = azurerm_resource_group.main.location
  size                            = var.vm_size2
  admin_username                  = var.admin_username2
  admin_password                  = var.admin_password2
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.azurevm1nic.id,  # Reference to the NIC you've defined
  ]

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}






# Creating db-vm:

resource "azurerm_public_ip" "azurevm2publicip" {
  name                = "pip-db"
  location            = var.location2
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
  sku                 = var.public_ip_sku2
}

resource "azurerm_network_interface" "azurevm2nic" {
  name                = "db-nic"
  location            = var.location2
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "ipconfig-db"
    subnet_id                     = azurerm_subnet.db_subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.azurevm2publicip.id
  }
}

# resource "tls_private_key" "db_ssh" {
#     algorithm = "RSA"
#     rsa_bits  = 4096
#   }
  
# resource "local_file" "db_public_key" {
#   filename = "${path.module}\\db_key.pem"
#   content =tls_private_key.db_ssh.private_key_pem
# }

resource "azurerm_linux_virtual_machine" "db-vm" {
  depends_on = [ azurerm_public_ip.azurevm2publicip ]
  name                            = "db-vm"
  resource_group_name             = azurerm_resource_group.main.name
  location                        = azurerm_resource_group.main.location
  size                            = var.vm_size2
  admin_username                  = var.admin_username2
  admin_password                  = var.admin_password2
  disable_password_authentication = false
  network_interface_ids = [
    azurerm_network_interface.azurevm2nic.id,  # Reference to the NIC you've defined
  ]





  # provisioner "remote-exec" {
  #   connection {
  #     type= "ssh"
  #     host = azurerm_public_ip.azurevm2publicip.ip_address
  #     user= var.admin_username2
  #     private_key = file("${path.module}/db_key.pem")  # Path to your private key PEM file for the "db-vm"
  #   }
  #   inline = [
  #     "echo 'hello' > test.txt",
  #     "sleep 10",
  #     "sudo apt-get update",
  #     "sudo apt-get install -y postgresql postgresql-client",
  #     "sudo service postgresql start",
  #     "sleep 10"

  #   ]

  # }
  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

  #  admin_ssh_key {
  #   username   = var.admin_username2
  #   public_key = tls_private_key.db_ssh.public_key_openssh  # Updated to use "db_ssh"
  # }

}

resource "azurerm_virtual_machine_extension" "db_script" {
  name                 = "db-vm-extension"
  virtual_machine_id   = azurerm_linux_virtual_machine.db-vm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.1"

  settings = <<SETTINGS
{
   "script": "${base64encode(file("${path.module}//db-vm-script.sh"))}"
}
SETTINGS

  depends_on = [
    azurerm_linux_virtual_machine.db-vm
  ]
}


resource "azurerm_virtual_machine_extension" "web_script" {
  name                 = "web-vm-extension"
  virtual_machine_id   = azurerm_linux_virtual_machine.web-vm.id
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"

  settings = <<SETTINGS
{
  "script": "${base64encode(file("${path.module}//web-vm-flask-script.sh"))}"
}
SETTINGS

  depends_on = [
    azurerm_linux_virtual_machine.web-vm,
    azurerm_virtual_machine_extension.db_script,
     azurerm_linux_virtual_machine.db-vm

  ]
}


# resource "azurerm_virtual_machine_extension" "db_script" {
#   name                 = "db-vm-extension"  # Unique name for the "db-vm" extension
#   virtual_machine_id   = azurerm_linux_virtual_machine.main2.id
#   publisher            = "Microsoft.Azure.Extensions"
#   type                 = "CustomScript"
#   type_handler_version = "2.0"

#   settings = <<SETTINGS
# {
#   "script": "${base64encode(file("${path.module}//db-vm-script.sh"))}"
# }
# SETTINGS

#   depends_on = [
#     azurerm_linux_virtual_machine.main2
#   ]
# }




































  









































