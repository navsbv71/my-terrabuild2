variable "resource_group_name2" {
  description = "Name of the Azure Resource Group"
  type        = string
  default     = "rg-terraform2"
}

variable "location2" {
  description = "Location for the Azure resources"
  type        = string
  default     = "westeurope"
}

variable "vnet_address_space2" {
  description = "Address space for the virtual network"
  type        = list(string)
  default     = ["10.0.0.0/16"]
}

variable "web_subnet_address_prefixes2" {
  description = "Address prefixes for the web subnet"
  type        = list(string)
  default     = ["10.0.1.0/24"]
}

variable "db_subnet_address_prefixes2" {
  description = "Address prefixes for the database subnet"
  type        = list(string)
  default     = ["10.0.2.0/24"]
}

variable "allowed_ssh_ip2" {
  description = "IP address allowed for SSH access"
  type        = string
  default     = "*"
}

variable "public_ip_sku2" {
  description = "SKU for the public IP address"
  type        = string
  default     = "Basic"
}

variable "vm_size2" {
  description = "Size of the virtual machine"
  type        = string
  default     = "Standard_F2"
}

variable "admin_username2" {
  description = "Admin username for the virtual machine"
  type        = string
  sensitive = true
  default     = "<insert  username>"
}

variable "admin_password2" {
  description = "Admin password for the virtual machine"
  type        = string
  sensitive = true
  default     = "<insert  password>"
}

variable "admin_subscription_id" {
  description = "Admin subscriptin for the cloud provider"
  type        = string
  sensitive = true
  default     = "<insert your subscription_id>"
}
